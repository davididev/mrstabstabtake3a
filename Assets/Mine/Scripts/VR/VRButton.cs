﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class VRButton : MonoBehaviour
{
    public Animator anim;
    public UnityEvent OnPressEvent;
    private float lastHoverTime = 0f;

    private void Update()
    {
        float dif = Time.time - lastHoverTime;

        anim.SetBool("Hover", (dif < 0.05f));
    }

    public void OnHover(Vector3 touchPos)
    {
        lastHoverTime = Time.time;
    }

    public void OnPress()
    {
        anim.SetTrigger("Press");
        OnPressEvent.Invoke();
    }
}
