using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayTime : MonoBehaviour
{
    private TMPro.TextMeshProUGUI textObj;
    private float timer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (textObj == null)
            textObj = GetComponent<TMPro.TextMeshProUGUI>();
        if (timer <= 0f)
        {
            timer = 1f;
            System.DateTime current = System.DateTime.Now;
            int h = current.Hour;
            int s = current.Minute;

            bool isPM = false;
            if(h >= 12)
            {
                h -= 12;
                isPM = true;
            }

            if(isPM)
            {
                if(h == 0)
                    textObj.text = "12:" + s.ToString("D2") + " PM ";
                else
                    textObj.text = h + ":" + s.ToString("D2") + " PM ";
            }
            if (!isPM)
            {
                if (h == 0)
                    textObj.text = "12:" + s.ToString("D2") + " AM ";
                else
                    textObj.text = h + ":" + s.ToString("D2") + " AM ";
            }
        }
        else
        {
            timer -= Time.unscaledDeltaTime;
        }
    }
}
