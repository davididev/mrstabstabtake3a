using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoilerRoot : MonoBehaviour
{
    public static int Count = 0;
    public static bool firstLoad = true;
    // Start is called before the first frame update
    void Awake()
    {
        Object.DontDestroyOnLoad(transform);
        if (Application.isEditor)
            {
                //Load the save data here if needed.  You can remove "isEditor" if you don't add titles.
            }
        Count++;
        if(firstLoad)  //Only autoload once (at the start of the game).
        {
            try
            {
                GameDataHolder.LoadFile(0);
            }
            catch(System.Exception e)
            {
                GameDataHolder.NewFile();
            }
            firstLoad = false;
        }
    }

    private void OnDestroy()
    {
        Count--;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
