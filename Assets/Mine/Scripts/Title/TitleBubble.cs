using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleBubble : MonoBehaviour
{
    public int WorldID = -1, LevelID = -1;

    public Transform textObj;
    public GameObject lockIcon;
    public GameObject childrenHolder; 

    float lockTimer = 0f;

    private void OnEnable()
    {
        transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        
        iTween.ScaleTo(gameObject, new Vector3(100f, 100f, 100f), 1f);

        lockIcon.SetActive(false);

        if(WorldID == -1 && LevelID == -2)
            lockIcon.SetActive(true);  //Locked until you sign your name

        if (WorldID > -1)  //Is not party mode- check for locks
        {
            if(LevelID == -1)
            {
                if (GameDataHolder.instance.worlds[WorldID].isUnlocked == false)
                    lockIcon.SetActive(true);
            }
            if (LevelID > GameData.GetMaxLevel(WorldID))  //You need to clear lower levels before you can do this one
                lockIcon.SetActive(true);
        }


    }

    //Referenced by Grip Controller
    void OnGrip(bool isLeftHand)
    {
        if (lockIcon.activeSelf == true)
        {
            if (lockTimer <= 0f)
            {
                float t = 0.2f;
                iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(50f, 50f, 50f), "time", t, "delay", 0f));
                iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(100f, 100f, 100f), "time", t, "delay", t));
                lockTimer = t * 2f;
            }
            return;
        }
        DialogueChangeScene[] teleportDialogue = new DialogueChangeScene[1];
        teleportDialogue[0] = new DialogueChangeScene();

        if(WorldID == -1 && LevelID == -1)
        {
            //Go to party mode
            PlayerMovement.GetInstance().TeleportWithNewBoiler("PartyTitle", Vector3.zero);


        }
        if(WorldID == -1 && LevelID == -2)
        {
            //Go to real level in Party Mode
            teleportDialogue[0].sceneToMoveTo = "PartyLevel";
            teleportDialogue[0].breakpoint = true;
            teleportDialogue[0].positionToMoveTo = Vector3.zero;
            Texture2D newTexture = Object.Instantiate(FindObjectOfType<PartySign>().signTexture) as Texture2D;
            
            PartyData.TempTexture = newTexture;
            DialogueHandler.RunEvent(teleportDialogue);
        }
        if(WorldID > -1 && LevelID == -1)
        {
            //Is a world bubble
            childrenHolder.SetActive(true);
            iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(50f, 50f, 50f), "time", 0.2f, "oncomplete", "DeactiveParent"));
            

        }
        if(WorldID > -1 && LevelID > -1)
        {
            //Is a level bubble.  
            PlayerMovement.GetInstance().TeleportWithNewBoiler(("W" + WorldID + "L" + LevelID), Vector3.zero);

        }
    }

    void DeactiveParent()
    {
        transform.parent.gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        textObj.LookAt(CrossVR.CurrentHMD.position);
        if(lockIcon.activeSelf)
            lockIcon.transform.LookAt(CrossVR.CurrentHMD.position);

        if (lockTimer > 0f)
            lockTimer -= Time.deltaTime;


        if (WorldID == -1 && LevelID == -2)
            lockIcon.SetActive(!PartySign.StartedSign);
    }
}
