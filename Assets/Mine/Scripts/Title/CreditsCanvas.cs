using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsCanvas : MonoBehaviour
{
    public TMPro.TextMeshProUGUI creditsText;
    public SpriteRenderer rend;
    public TextAsset creditsFile;


    private const int SECONDS_BETWEEN_LINES = 12;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CreditsRoutine());
    }

    IEnumerator CreditsRoutine()
    {
        string[] lines = creditsFile.text.Split('*');


        while(gameObject)
        {
            for(int i = 0; i < lines.Length; i++)
            {
                int secondsLeft = SECONDS_BETWEEN_LINES;
                while(secondsLeft > 0)
                {
                    if (rend.isVisible)
                        secondsLeft--;

                    creditsText.text = "Credits: \n" + lines[i];

                    yield return new WaitForSeconds(1f);
                }
                
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
