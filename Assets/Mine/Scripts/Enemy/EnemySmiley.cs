using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySmiley : Enemy
{
    public override IEnumerator MainRoutine()
    {
        bool foundPlayer = false;
        while(!foundPlayer)
        {
            anim.SetTrigger("Jump");
            ccm.Jump(2f);

            while (ccm.cc.isGrounded == false)
            {
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForEndOfFrame();

            anim.SetTrigger("Land");

            if (Vector3.Distance(transform.position, PlayerMovement.PlayerPos) < 10f)
                foundPlayer = true;
            else
                yield return new WaitForSeconds(2f);

            if (Vector3.Distance(transform.position, PlayerMovement.PlayerPos) < 10f)
                foundPlayer = true;
        }

        while(gameObject)
        {
            bool turned = false;
            while(turned == false)
            {
                turned = TurnTowardsPoint(PlayerMovement.PlayerPos, 180f);
                yield return new WaitForEndOfFrame();
            }

            anim.SetTrigger("Jump");
            ccm.Jump(2f);
            ccm.moveVec = Vector2.up;

            yield return new WaitForSeconds(0.1f);
            
            
            while (ccm.cc.isGrounded == false)
            {
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForEndOfFrame();

            anim.SetTrigger("Land");
            ccm.moveVec = Vector2.zero;
            yield return new WaitForSeconds(2f);
        }
    }
}
