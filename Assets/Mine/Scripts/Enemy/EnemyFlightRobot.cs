using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlightRobot : Enemy
{

    public BoxCollider bladeCollider;

    public override void OnStart()
    {
        base.OnStart();
        ccm.internalGravityScale = 0f;
    }

    public override void OnUpdate()
    {
        Collider[] cs = PhysicsExtensions.OverlapBox(bladeCollider, LayerMask.GetMask("Player"));
        foreach(Collider c in cs)
        {
            c.gameObject.SendMessage("Damage", new DmgMessage(1f, transform.forward * 1.5f));
        }

        TurnTowardsPoint(PlayerMovement.PlayerPos, 360f);
    }

    public override IEnumerator MainRoutine()
    {

        while(gameObject)
        {
            Vector3 playerHead = PlayerMovement.PlayerPos + (Vector3.up * 1.5f);
            Vector3 v = (playerHead - transform.position).normalized;

            if (v.y < -1f)
                v.y = -1f;
            if (v.y > 1f)
                v.y = 1f;

            float multiplier = 1.3f;
            if (Vector3.Distance(playerHead, transform.position) < 2f)
                multiplier = 0.35f;
            Debug.Log("Force: " + v + " x " + multiplier);
            ccm.AddForce(v * multiplier, 1f);
            yield return new WaitForSeconds(0.75f);
        }
    }
}
