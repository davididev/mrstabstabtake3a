using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private MeshRenderer[] meshRends;
    [SerializeField] private SkinnedMeshRenderer[] skinMeshRends;

    private CharacterControllerMovement ccm;

    private Color emissionColor = Color.black;

    private Color targetColor = Color.black;

    public bool isInvincible = false;

    public float maxHealth = 4f;
    private float health;

    private bool runInvincible;

    public float HealthPerc
    {
        get { return health / maxHealth; }
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        health = maxHealth;
        ccm = GetComponent<CharacterControllerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ccm == null)
            ccm = GetComponent<CharacterControllerMovement>();
        emissionColor.r = Mathf.MoveTowards(emissionColor.r, targetColor.r, Time.deltaTime * 2f);
        emissionColor.g = Mathf.MoveTowards(emissionColor.g, targetColor.g, Time.deltaTime * 2f);
        emissionColor.b = Mathf.MoveTowards(emissionColor.b, targetColor.b, Time.deltaTime * 2f);

        if (emissionColor == Color.red)
            targetColor = Color.black;

        for(int i = 0; i < meshRends.Length; i++)
        {
            meshRends[i].material.SetColor("_EmissionColor", emissionColor);
        }
        for (int i = 0; i < skinMeshRends.Length; i++)
        {
            skinMeshRends[i].material.SetColor("_EmissionColor", emissionColor);
        }
        runInvincible = isInvincible;
        if (!PlayerWeapon.StartedLevel)
            runInvincible = true;

        if (runInvincible)
        {
            if (emissionColor == Color.black)
                targetColor = Color.white;
            if (emissionColor == Color.white)
                targetColor = Color.black;
        }

        if (!runInvincible && targetColor == Color.white)
            targetColor = Color.black;

    }

    public void Damage(DmgMessage msg)
    {
        if(emissionColor == Color.black)
        {
            health -= msg.DamageAmount;
            
            targetColor = Color.red;

            if(health <= 0f)
            {
                gameObject.SendMessage("OnDeath");
            }
        }
        else
        {
            if (PromptNotif.FirstInvincible == false && runInvincible)
            {
                HealthOverlay.GuideLabel = "Can't hurt enemy if flashing white.";
                PromptNotif.FirstInvincible = true;
            }
        }
        
    }
}
