using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float Damage = 1f;
    public SphereCollider sphere;
    public float MovePerSecond = 3f;
    public Rigidbody rigid;

    public AudioClip onSpawnSoundFX;


    public TrailRenderer trailRend;
    // Start is called before the first frame update
    void OnEnable()
    {
        if (trailRend != null)
            trailRend.Clear();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rigid.velocity = transform.forward * MovePerSecond;
        Collider[] cols = PhysicsExtensions.OverlapSphere(sphere, LayerMask.GetMask("Player"));
        foreach (Collider c in cols)
        {

            c.gameObject.SendMessage("Damage", new DmgMessage(Damage, transform.forward, transform));
            gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        gameObject.SetActive(false);
    }
}
