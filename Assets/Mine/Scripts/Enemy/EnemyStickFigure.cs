using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStickFigure : Enemy
{
    public const float TURN_SPEED = 180f, BULLET_TIME = 1f;
    public GameObject bulletPrefab;
    public Transform bulletPoint;
    public AudioClip shootFX;
    public override void OnStart()
    {
        base.OnStart();
    }
    public override IEnumerator MainRoutine()
    {
        yield return new WaitForEndOfFrame();
        GameObjectPool.InitPoolItem("Stick Bullet", bulletPrefab, 90);

        float bulletTimer = 0f;
        while(gameObject)
        {
            float distance = Vector3.Distance(transform.position, PlayerMovement.PlayerPos);
            if (distance > 2f)
            {
                anim.SetBool("Walk", true);
                ccm.moveVec = new Vector2(0f, 1f);
                this.TurnTowardsPoint(PlayerMovement.PlayerPos, TURN_SPEED);
            }
            else
            {
                anim.SetBool("Walk", false);
                ccm.moveVec = new Vector2(0f, 0f);
                this.TurnTowardsPoint(PlayerMovement.PlayerPos, TURN_SPEED);
            }
            bulletTimer += Time.deltaTime;
            if(bulletTimer >= BULLET_TIME)
            {
                GameObject bul = GameObjectPool.GetInstance("Stick Bullet", bulletPoint.position, Quaternion.identity);
                if(bul != null)
                {
                    audioSrc.PlayOneShot(shootFX);
                    bul.transform.forward = transform.forward;
                    
                }

                bulletTimer -= BULLET_TIME;

            }
            yield return new WaitForEndOfFrame();
        }
        
    }
}
