using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBomber : Enemy
{
    public GameObject explosionPrefab;
    public const float EXPLODE_DIST = 4.5f;

    public override void OnStart()
    {
        GameObjectPool.InitPoolItem("Bomber Explosion", explosionPrefab, 15);
    }
    public override IEnumerator MainRoutine()
    {
        while(gameObject)
        {
			ccm.waterVeritcal = 1f;
            this.TurnTowardsPoint(PlayerMovement.PlayerPos, 360f);
            ccm.moveVec = new Vector2(0f, 1f);

            float dist = Vector3.Distance(transform.position, PlayerMovement.PlayerPos);
            if(dist < EXPLODE_DIST)
            {
				if (PromptNotif.FirstBomberExplode == false)
				{
					PromptNotif.FirstBomberExplode = true;
					HealthOverlay.GuideLabel = "Throw your weapon at the bomber to avoid getting hit by the explosion.";
				}
                gameObject.SendMessage("Damage", new DmgMessage(2f, Vector3.zero, null));
                
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public void OnDeath()
    {
        base.OnDeath();

        GameObject g = GameObjectPool.GetInstance("Bomber Explosion", transform.position, Quaternion.identity);
        Collider[] cs = Physics.OverlapSphere(transform.position, EXPLODE_DIST);
        foreach (Collider c in cs)
        {
            c.gameObject.SendMessage("Damage", new DmgMessage(2f, Vector3.zero, null));
        }  
	}
}
