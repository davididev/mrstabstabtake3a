using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodDrop : MonoBehaviour
{
    [System.Serializable]
    public struct FoodInfo
    {
        public Sprite img;
        public string description;
    }
    public SpriteRend3D rend;

    string descToDisplay;
    int points;

    [SerializeField] private FoodInfo[] onePoint;
    [SerializeField] private FoodInfo[] fivePoint;
    [SerializeField] private FoodInfo[] twentyPoint;
    [SerializeField] private FoodInfo[] fiftyPoint;

    private float expireTimer = 0f;

    public AudioClip munchSound;

    // Start is called before the first frame update
    void OnEnable()
    {
        transform.localScale = Vector3.one;
        expireTimer = 60f;

        if (PromptNotif.FirstFoodSpawn == false)
        {
            promptTimer = 0.1f;  //Don't do it instantly- it may interfere with GameObjectPool's original init

        }
    }

    public void Setup(int pnt)
    {
        points = pnt;
        int id = Random.Range(0, 2);
        if (points == 1)
        {
            rend.rend0 = onePoint[id].img;
            descToDisplay = onePoint[id].description + " +" + points;
        }
        if (points == 5)
        {
            rend.rend0 = fivePoint[id].img;
            descToDisplay = fivePoint[id].description + " +" + points;
        }
        if (points == 20)
        {
            rend.rend0 = twentyPoint[id].img;
            descToDisplay = twentyPoint[id].description + " +" + points;
        }
        if (points == 50)
        {
            rend.rend0 = fiftyPoint[id].img;
            descToDisplay = fiftyPoint[id].description + " +" + points;
        }

        RaycastHit myInfo;
        if(Physics.Raycast(transform.position, Vector3.down, out myInfo, 100f, LayerMask.GetMask("Default")))
        {
            Vector3 pt = myInfo.point + (Vector3.up * 0.5f);
            iTween.MoveTo(gameObject, pt, 3f);
        }
    }

	float promptTimer = 0f;

    // Update is called once per frame
    void Update()
    {
		if(promptTimer > 0f)
		{
			promptTimer -= Time.deltaTime;
			if(promptTimer <= 0f && PromptNotif.FirstFoodSpawn == false)
			{
				HealthOverlay.GuideLabel = "Eat food for points and to renew magic power.";
				PromptNotif.FirstFoodSpawn = true;
			}
		}
        expireTimer -= Time.deltaTime;
        if(expireTimer < 0f)
        {
            float t = 6f;
            iTween.ScaleTo(gameObject, new Vector3(0.01f, 0.01f, 0.01f), t);
            Invoke("DisableMe", t);
        }

        float dist = Vector3.Distance(CrossVR.CurrentHMD.position, transform.position);
        if(dist < 1.5f)
        {
            HealthOverlay.IndivPointsLabel = descToDisplay;
            PlayerHealth.Score += points;
            PlayerHealth.Magic += 0.35f;  //3 of these restores one magic
            if (PlayerHealth.Magic > PlayerHealth.MAX_MAGIC)
                PlayerHealth.Magic = PlayerHealth.MAX_MAGIC;
            gameObject.SetActive(false);

            AudioSource.PlayClipAtPoint(munchSound, CrossVR.CurrentHMD.position);
        }
    }

    void DisableMe()
    {
        gameObject.SetActive(false);
    }
}
