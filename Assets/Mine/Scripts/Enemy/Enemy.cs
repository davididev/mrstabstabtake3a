using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyHealth))]
[RequireComponent(typeof(CharacterControllerMovement))]
public class Enemy : MonoBehaviour
{
    private Coroutine mainRoutine;
    protected EnemyHealth health;
    protected CharacterControllerMovement ccm;
    protected Animator anim;
    public float touchDamage = 1f;

    protected bool firstDeath = false;

    public int points = 7;

    private bool startedRoutine = false;

    public static int Count = 0;

    public AudioClip damageSound;

    protected AudioSource audioSrc;

    // Start is called before the first frame update
    void OnEnable()
    {
        Count++;
        firstDeath = true;
        startedRoutine = false;
        if (health == null)
            health = GetComponent<EnemyHealth>();
        if (ccm == null)
            ccm = GetComponent<CharacterControllerMovement>();
        if (anim == null)
            anim = GetComponent<Animator>();
        
        if(audioSrc == null)
        {
            audioSrc = gameObject.AddComponent<AudioSource>();
            audioSrc.minDistance = 5f;
            audioSrc.maxDistance = 25f;
            audioSrc.spatialBlend = 1f;
        }

        OnStart();
    }

    public void OnDeath()
    {
        //Points and stuff

        GameObject ded = GameObjectPool.GetInstance("Ded", transform.position, transform.rotation);

        if(firstDeath == true)
            firstDeath = false;

        int tempPoint = points;
        while(tempPoint > 0)
        {
            int t = 0;
            if (tempPoint >= 1)
                t = 1;
            if (tempPoint >= 5)
                t = 5;
            if (tempPoint >= 20)
                t = 20;
            if (tempPoint >= 50)
                t = 50;
            
            
            

            if (t > 0)
            {
                Vector3 v = transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(0f, 1f), Random.Range(-1f, 1f));
                GameObject g = GameObjectPool.GetInstance("Food", v, Quaternion.identity);
                g.SendMessage("Setup", t);
                tempPoint -= t;
            }
        }

        gameObject.SetActive(false);
    }

    /// <summary>
    /// Turn towards a point
    /// </summary>
    /// <param name="pt">Point to turn towards</param>
    /// <param name="rotateSpeed">How many degrees to rotate per second</param>
    /// <returns>True if it has fully turned to the point, false if it's still turning</returns>
    protected bool TurnTowardsPoint(Vector3 pt, float rotateSpeed)
    {
        float targetY = Quaternion.LookRotation(pt - transform.position).eulerAngles.y;
        Vector3 currentRot = transform.eulerAngles;
        currentRot.y = Mathf.MoveTowardsAngle(currentRot.y, targetY, rotateSpeed * Time.deltaTime);
        transform.eulerAngles = currentRot;
        if (Mathf.Approximately(currentRot.y, targetY))
            return true;
        else
            return false;
    }

    public virtual void OnStart()
    {

    }

    public virtual void OnUpdate()
    {

    }

    private void OnDisable()
    {
        Count--;
        if(mainRoutine != null)
            StopCoroutine(mainRoutine);
    }

    public virtual IEnumerator MainRoutine()
    {
        yield return null;
    }

    // Update is called once per frame
    void Update()
    {
        if (health == null)
            health = GetComponent<EnemyHealth>();
        if (ccm == null)
            ccm = GetComponent<CharacterControllerMovement>();
        if (anim == null)
            anim = GetComponent<Animator>();
        OnUpdate();

        if(startedRoutine == false)
        {
            if(PlayerWeapon.StartedLevel)
            {
                mainRoutine = StartCoroutine(MainRoutine());
                startedRoutine = true;
            }
        }

        if(damageSoundTimer > 0f)
        {
            damageSoundTimer -= Time.deltaTime;
        }
    }

    public virtual void OnControllerCollide(ControllerColliderHit hit)
    {

    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        OnControllerCollide(hit);
        if(touchDamage > 0f)
        {
            if (hit.gameObject.tag == "Player")
                hit.gameObject.SendMessage("Damage", new DmgMessage(touchDamage, transform.forward));
        }
        
    }

    float damageSoundTimer = 0f;

    void Damage(DmgMessage msg)
    {
        if(damageSoundTimer <= 0f)
        {
            ccm.AddForce(msg.DamageForce, 1f);
            audioSrc.PlayOneShot(damageSound);
            damageSoundTimer = damageSound.length;
        }
            
    }

    void ControlHit(ControllerMessage msg)
    {
        if(msg.origin.tag == "Player")
        {
            msg.origin.SendMessage("Damage", new DmgMessage(1f, transform.forward * 1.5f, transform));
        }
    }
}
