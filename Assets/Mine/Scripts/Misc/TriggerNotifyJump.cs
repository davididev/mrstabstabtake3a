using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerNotifyJump : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	void OnTriggerEnter(Collider c)
	{
		if(c.gameObject.tag == "Player")
		{
			if (PromptNotif.FirstJump== false)
			{
				PromptNotif.FirstJump = true;
				HealthOverlay.GuideLabel = "Jump in place to reach the higher areas.";
			}
		}
	}
}
