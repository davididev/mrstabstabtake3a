﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerMessage
{
    public ControllerColliderHit info;
    public GameObject origin;

    public ControllerMessage(ControllerColliderHit i, GameObject o)
    {
        info = i;
        origin = o;
    }
}
