using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartyHighScoreCanvas : MonoBehaviour
{
    public RawImage originalRawImage;
    public TMPro.TextMeshProUGUI originalTextPro;
    private const float Y_PER_ITERATION = -40f;
    // Start is called before the first frame update
    void Start()
    {
        SortedSet<PartyData.HighScoreItem>.Enumerator e1 = PartyData.Instance.Entries.GetEnumerator();

        originalRawImage.gameObject.SetActive(false);
        originalTextPro.gameObject.SetActive(false);
        int i = 0;
        while (e1.MoveNext())
        {

            if (i == 0)
            {
                originalRawImage.gameObject.SetActive(true);
                originalTextPro.gameObject.SetActive(true);
                originalRawImage.texture = e1.Current.Signature;
                originalTextPro.text = e1.Current.Score.ToString("D6");
            }
            else
            {
                GameObject newRawImage = GameObject.Instantiate(originalRawImage.gameObject) as GameObject;
                GameObject newTextPro = GameObject.Instantiate(originalTextPro.gameObject) as GameObject;
                newRawImage.transform.parent = originalRawImage.transform.parent;
                newTextPro.transform.parent = originalTextPro.transform.parent;
                newRawImage.transform.localPosition = originalRawImage.transform.localPosition;
                newTextPro.transform.localPosition = originalTextPro.transform.localPosition;
                newRawImage.transform.localScale= originalRawImage.transform.localScale;
                newTextPro.transform.localScale = originalTextPro.transform.localScale;
                newRawImage.transform.localEulerAngles = originalRawImage.transform.localEulerAngles;
                newTextPro.transform.localEulerAngles = originalTextPro.transform.localEulerAngles;

                //newRawImage.GetComponent<RectTransform>().Translate(new Vector3(0f, Y_PER_ITERATION * i, 0f), Space.World);
                //newTextPro.GetComponent<RectTransform>().Translate(new Vector3(0f, Y_PER_ITERATION * i, 0f), Space.World);

                Vector3 v1 = newRawImage.transform.localPosition;
                v1.y += Y_PER_ITERATION * i;
                newRawImage.transform.localPosition = v1;
                v1 = newTextPro.transform.localPosition;
                v1.y += Y_PER_ITERATION * i;
                newTextPro.transform.localPosition = v1;

                newRawImage.GetComponent<RawImage>().texture = e1.Current.Signature;
                newTextPro.GetComponent<TMPro.TextMeshProUGUI>().text = e1.Current.Score.ToString("D6");
            }
            i++;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
