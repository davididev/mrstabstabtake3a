using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartySpawn : MonoBehaviour
{
    [System.Serializable]
    public struct Entry
    {
        public GameObject enemyPrefab;
        public int weight;
    }
    public Entry[] list;
    public string firstWave = "0 0 1 2";

    private int maxWeight = 0;


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < list.Length; i++)
        {
            GameObjectPool.InitPoolItem("Enemy" + i, list[i].enemyPrefab, 20);
            maxWeight += list[i].weight;
        }

        StartCoroutine(SpawnEnemies());
    }

    void SpawnEnemy(int i, int enemyIDOverride = -1)
    {
        Random.InitState(i);
        int randoWeight = Random.Range(1, maxWeight);
        int actualID = 0;

        if (enemyIDOverride != -1)
            actualID = enemyIDOverride;
        else
        {
            for(int w = 0; w < list.Length; w++)
            {
                randoWeight -= list[w].weight;
                if (randoWeight <= 0f) 
                    break;
                else
                    actualID++;
            }
        }
		if(actualID > list.Length)
			actualID = list.Length;

        int xSeed = i * 2;
        int zSeed = i * 3;
        Random.InitState(xSeed);
        float x = Random.Range(-20f, 20f);
        Random.InitState(zSeed);
        float z = Random.Range(-8f, 8f);

        GameObject g = GameObjectPool.GetInstance("Enemy" + actualID, new Vector3(x, 9f, z), Quaternion.identity);
    }

    IEnumerator SpawnEnemies()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        while (PlayerWeapon.StartedLevel == false)
        {
            yield return new WaitForEndOfFrame();
        }
        int maxCount = 3;
        float SPAWN_TIME = 5f;
        int seed = 0;


        while (gameObject.activeSelf)
        {

            string[] numbersToParse = firstWave.Split(' ');
            for(int w = 0; w < numbersToParse.Length; w++)
            {
                while (Enemy.Count > maxCount)
                {
                    yield return new WaitForSeconds(1f);
                }
                int overrideID = int.Parse(numbersToParse[w]);
                SpawnEnemy(seed, overrideID);
                seed++;

                yield return new WaitForSeconds(8f);
            }

            while(gameObject)
            {
                seed++;
                while(Enemy.Count > maxCount)
                {
                    yield return new WaitForSeconds(1f);
                }
                SpawnEnemy(seed);
                yield return new WaitForSeconds(SPAWN_TIME);

                if(seed % 10 == 0)
                {
                    SPAWN_TIME *= 0.8f;
                    maxCount++;
                }
            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
