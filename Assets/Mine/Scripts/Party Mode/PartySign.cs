using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartySign : MonoBehaviour
{
    public MeshRenderer rend;
    public static bool StartedSign = false;
    [HideInInspector] public Texture2D signTexture;
    // Start is called before the first frame update
    void Start()
    {
        PromptNotif.ResetVars();
        PlayerMovement.standingY = 0f;  //You passed the headset over to the next player- set height to recalibrate.
        StartedSign = false;
        signTexture = new Texture2D(256, 256);
        for(int x = 0; x < 256; x++)
        {
            for(int y = 0; y < 256; y++)
            {
                signTexture.SetPixel(x, y, Color.white);
            }
        }
        signTexture.Apply();
        rend.material.SetTexture("_MainTex", signTexture);
    }

    public void TexturePaint(Vector2 scanPosition, Color c)
    {
        int x = Mathf.FloorToInt(scanPosition.x * 256f);
        int y = Mathf.FloorToInt(scanPosition.y * 256f);

        for (int i = x - 3; i < x + 3; i++)
        {
            for (int u = y - 3; u < y + 3; u++)
            {
				if(i >= 0  && i < 256)
				{
					if(u >= 0  && u < 256)
					{
						signTexture.SetPixel(i, u, c);
					}
				}
                
            }
        }
        signTexture.Apply();
        rend.material.SetTexture("_MainTex", signTexture);
        StartedSign = true;

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
