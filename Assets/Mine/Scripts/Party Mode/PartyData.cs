using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyData 
{
    public class HighScoreItem
    {
        public int Score = 0;
        public Texture2D Signature;
        public HighScoreItem(int s, Texture2D tex)
        {
            Score = s;
            Signature = tex;
        }
    }

    public class HighScoreItemComparar : IComparer<HighScoreItem>
    {
        public int Compare(HighScoreItem x, HighScoreItem y)
        {
            return y.Score.CompareTo(x.Score);
        }
    }


    public static Texture2D TempTexture;

    public SortedSet<HighScoreItem> Entries;

    public static PartyData Instance = new PartyData();

    public PartyData()
    {
        Entries = new SortedSet<HighScoreItem>(new HighScoreItemComparar());
    }

    
    public void AddHighScore(int amt)
    {
        if (TempTexture == null)
            TempTexture = new Texture2D(256, 256);
        Texture2D newTexture = Object.Instantiate(TempTexture) as Texture2D;
        Entries.Add(new HighScoreItem(amt, newTexture));
    }

}
