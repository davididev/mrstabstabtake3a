using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyBrush : MonoBehaviour
{
    private const float SCAN_DIST = 6.3f;
    public MeshRenderer meshRend;
    private Color brushColor = Color.white;
    private Color colorTarget = Color.black;
    private Color currentColor;
    private const float COLOR_MOVE = 0.5f;

    public LineRenderer lineRend;
    public SpriteRenderer pointer;
    public Transform origin;

    private bool isHolding = true;

    // Start is called before the first frame update
    void Start()
    {
        lineRend.gameObject.SetActive(false);
        pointer.gameObject.SetActive(false);
    }

    void OnGrip(bool leftHand)
    {
        lineRend.gameObject.SetActive(true);
        pointer.gameObject.SetActive(true);
        isHolding = true;
        colorTarget = Color.black;
        currentColor = Color.black;
        meshRend.materials[0].SetColor("_Color", currentColor);
    }

    void OnRelease(bool leftHand)
    {
        lineRend.gameObject.SetActive(false);
        pointer.gameObject.SetActive(false);
        isHolding = false;
        currentColor = Color.white;
        meshRend.materials[0].SetColor("_Color", currentColor);

    }

    void OnTriggerRelease()
    {
        isPressed = false;
    }

    void OnTrigger()
    {
        isPressed = true;
    }

    void SetTargetColor()
    {
        if (colorTarget == Color.black)
        {
            colorTarget = Color.red;
            return;
        }
        if (colorTarget == Color.red)
        {
            colorTarget = Color.blue;
            return;
        }
        if (colorTarget == Color.blue)
        {
            colorTarget = Color.black;
            return;
        }
    }

    private bool isPressed = false;

    // Update is called once per frame
    void Update()
    {
        if (isHolding)
        {
            if (currentColor == colorTarget)
            {
                SetTargetColor();
            }
            else
            {
                currentColor.r = Mathf.MoveTowards(currentColor.r, colorTarget.r, COLOR_MOVE * Time.deltaTime);
                currentColor.g = Mathf.MoveTowards(currentColor.g, colorTarget.g, COLOR_MOVE * Time.deltaTime);
                currentColor.b = Mathf.MoveTowards(currentColor.b, colorTarget.b, COLOR_MOVE * Time.deltaTime);
                lineRend.startColor = currentColor;
                lineRend.endColor = currentColor;
                pointer.color = currentColor;
                meshRend.material.SetColor("_Color", currentColor);
            }

            RaycastHit info;
            Vector3 endLinePos = Vector3.zero;
            if (Physics.Raycast(origin.position, transform.forward, out info, SCAN_DIST * transform.localScale.x, LayerMask.GetMask("Bullet")))
            {
                endLinePos = info.point;
                if (isPressed)
                {
					
                    PartySign ps = info.collider.gameObject.GetComponent<PartySign>();
					
                    if (ps != null)
					{
						
						Vector2 uv = info.textureCoord;
                        ps.TexturePaint(uv, currentColor);
					}
                }

            }
            else
                endLinePos = origin.position + (transform.forward * SCAN_DIST);


            lineRend.SetPositions(new Vector3[]  { origin.position, endLinePos});
            pointer.transform.position = endLinePos;
            pointer.transform.LookAt(CrossVR.CurrentHMD.position);
                
        }
    }
}
