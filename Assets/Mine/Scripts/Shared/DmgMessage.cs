using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmgMessage 
{
    public float DamageAmount = 0f;
    public Vector3 DamageForce = Vector3.zero;
    public Transform source;

    public DmgMessage(float amt, Vector3 force, Transform src = null)
    {
        DamageAmount = amt;
        DamageForce = force;
        source = src;
    }
}
