﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
#if UNITY_EDITOR || UNITY_STANDALONE
using Valve.VR;
#endif

public class VR_walklocomotion : MonoBehaviour
{
    public CharacterControllerMovement ccm;
#if UNITY_EDITOR || UNITY_STANDALONE
    public SteamVR_Action_Pose leftFootPose, rightFootPose;
#endif
    private float leftStartY = 0f, rightStartY = 0f;
    public bool disconnected = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    public void SetStartingPositions()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        if (Application.isMobilePlatform == false)  //Don't calibrate on Android
        {
            leftStartY = leftFootPose.localPosition.y;
            rightStartY = rightFootPose.localPosition.y;
        }

        if (Application.isEditor)  //Don't calibrate on Android
        {
            leftStartY = leftFootPose.localPosition.y;
            rightStartY = rightFootPose.localPosition.y;
        }
#endif
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        disconnected = false;
        if (Mathf.Approximately(leftFootPose.localPosition.y, 0f) && Mathf.Approximately(rightFootPose.localPosition.y, 0f))
            disconnected = true;
        if (Mathf.Approximately(leftStartY, 0f) && Mathf.Approximately(rightStartY, 0f))
            disconnected = true;

        if (!disconnected)
        {
            bool lf = (leftFootPose.localPosition.y - leftStartY) > 0.2f;
            bool rf = (rightFootPose.localPosition.y - rightStartY) > 0.2f;
            bool fwd = false;
            if (lf == true && rf == false)
                fwd = true;
            if (lf == false && rf == true)
                fwd = true;


            if (fwd)
                ccm.moveVec = new Vector2(0f, 1f);
            else
                ccm.moveVec = new Vector2(0f, 0f);
        }
#endif
    }
}
