using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromptNotif
{
    public static bool FirstPreGrab = false;
    public static bool FirstInvincible = false;
    public static bool FirstTelekinesis = false;
    public static bool FirstFoodSpawn = false;
	public static bool FirstBomberExplode = false;
	public static bool FirstJump = false;
	public static bool FirstWater = false;

    /// <summary>
    /// Should only be called by party mode.
    /// </summary>
    public static void ResetVars()
    {
        FirstPreGrab = false;
        FirstInvincible = false;
        FirstTelekinesis = false;
        FirstFoodSpawn = false;
		FirstBomberExplode = false;
		FirstJump = false;
		FirstWater = false;
    }
}
