using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public const float MAX_HEALTH = 8f, MAX_MAGIC = 10f;
	public const float VIBRATE_THRESHOLD = 0.1f;  //How far in world units does it have to be from left/right to vibrate just one controller.
    public static float Health, Magic;
    public Image fadeImage;
    private bool death = false;

    public static float bubblesPerc = 1f;
    private const float UNDERWATER_TIME = 60 * 1.5f;  //1.5 minutes
    private float bubbleTimer = UNDERWATER_TIME;

    public static int Score = 0;
    // Start is called before the first frame update
    void Start()
    {
        SceneManager.activeSceneChanged += ResetHealth;
        ResetHealth(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
    }

    bool isDamaging = false;

    private void ResetHealth(Scene arg0, Scene arg1)
    {
        Health = MAX_HEALTH;
        Magic = MAX_MAGIC;
        Score = 0;
        death = false;
        HealthOverlay.IndivPointsLabel = " ";  //Re-activate the score label
    }

    public void Damage(DmgMessage msg)
    {
        if (isDamaging)
            return;
        if (death)
            return;

        float amp = 0.25f * msg.DamageAmount;
        if (msg.source == null)  //No source- vibrate both controllers
        {
            CrossVR.Vibrate(true, 0.5f, amp);
            CrossVR.Vibrate(false, 0.5f, amp);
        }
        else
        {
            Vector3 local = transform.InverseTransformPoint(msg.source.position);
            if(local.x < -VIBRATE_THRESHOLD)  //Vibrate left hand
                CrossVR.Vibrate(true, 0.5f, amp);
            if (local.x > VIBRATE_THRESHOLD)  //Vibrate right hand
                CrossVR.Vibrate(false, 0.5f, amp);
			
			if(local.x >= -VIBRATE_THRESHOLD && local.x <= VIBRATE_THRESHOLD)  //Very close to center- vibrate both
			{
				CrossVR.Vibrate(true, 0.5f, amp);
				CrossVR.Vibrate(false, 0.5f, amp);
			}
        }
        
        Health -= msg.DamageAmount;
        if (Health <= 0.1f)
        {
            death = true;
            Health = 0f;
            PartyData.Instance.AddHighScore(Score);
            //Dead code
            if(SceneManager.GetActiveScene().name == "PartyLevel")
            {
                DialogueChangeScene[] sc = new DialogueChangeScene[1];
                sc[0] = new DialogueChangeScene();
                sc[0].breakpoint = true;
                sc[0].sceneToMoveTo = "PartyTitle";
                sc[0].positionToMoveTo = Vector3.zero;

                PartyData.Instance.AddHighScore(Score);

                DialogueHandler.RunEvent(sc);
            }
        }

        StartCoroutine(DamageFlash());
    }

    IEnumerator DamageFlash()
    {
        isDamaging = true;
        Color c = new Color(1f, 0f, 0f, 0f);
        fadeImage.color = c;

        while(c.a < 1f)
        {
            c.a += 2f * Time.deltaTime;
            if (c.a > 1f)
                c.a = 1f;
            fadeImage.color = c;
            yield return new WaitForEndOfFrame();
        }

        while (c.a > 0f)
        {
            c.a -= 2f * Time.deltaTime;
            if (c.a < 0f)
                c.a = 0f;
            fadeImage.color = c;
            yield return new WaitForEndOfFrame();
        }
        isDamaging = false;
    }

    private void OnDestroy()
    {
        SceneManager.activeSceneChanged -= ResetHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if(CrossVR.CurrentHMD.position.y > WaterSurface.WaterY)
            bubbleTimer = UNDERWATER_TIME; 
        else
        {
            bubbleTimer -= Time.deltaTime;
            if(bubbleTimer <= -5f)  //Damage every 5 seconds when you run out of breath.
            {
                Damage(new DmgMessage(1f, Vector3.zero, transform));
                bubbleTimer = 0f;
            }
        }

        bubblesPerc = Mathf.Clamp(bubbleTimer / UNDERWATER_TIME, 0f, 1f);
    }
}
