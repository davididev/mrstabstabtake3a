using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthOverlay : MonoBehaviour
{
    

    public Image ringImage;
    public RectTransform magicBar;
    public TMPro.TextMeshProUGUI pointsText, guideText, enemyCountText;
    public TMPro.TextMeshProUGUI[] indivPointsText;
    private float[] indivPointsTimer;

    public GameObject bubblesOverlay;
    private float DEFAULT_BAR_HEIGHT = 0f;

    public RectTransform[] bubbleBars;

    public static string IndivPointsLabel = "", GuideLabel = "";  //Set this to something to activate one of the indiv points text.


    // Start is called before the first frame update
    void Start()
    {
        DEFAULT_BAR_HEIGHT = bubbleBars[0].sizeDelta.y;
        indivPointsTimer = new float[indivPointsText.Length];
    }

    // Update is called once per frame
    void Update()
    {
        float perc1 = PlayerHealth.Health / PlayerHealth.MAX_HEALTH;
        float perc2 = PlayerHealth.Magic / PlayerHealth.MAX_MAGIC;

        enemyCountText.text = "Enem: " + Enemy.Count;

        magicBar.localScale = new Vector3(perc2, 1f, 1f);
        ringImage.fillAmount = perc1;

        for(int i = 0; i < indivPointsTimer.Length; i++)
        {
            if (indivPointsTimer[i] > 0f)
                indivPointsTimer[i] -= Time.deltaTime;
        }

        if(GuideLabel != "")
        {
            guideText.text = GuideLabel;
            GuideLabel = "";
            guideText.GetComponent<Animator>().SetTrigger("Play");
            
        }

        bubblesOverlay.SetActive(!Mathf.Approximately(PlayerHealth.bubblesPerc, 1f));
        for(int x = 0; x < bubbleBars.Length; x++)
        {
            Vector2 rect = bubbleBars[x].sizeDelta;
            rect.y = DEFAULT_BAR_HEIGHT * PlayerHealth.bubblesPerc;
            bubbleBars[x].sizeDelta = rect;

        }

        
        if (IndivPointsLabel != "")
        {
            pointsText.text = "Score: " + PlayerHealth.Score.ToString("D5");
            for (int i = 0; i < indivPointsTimer.Length; i++)
            {
                
                if(indivPointsTimer[i] <= 0f)
                {
                    indivPointsText[i].text = IndivPointsLabel;
                    indivPointsTimer[i] = 2.1f;
                    indivPointsText[i].GetComponent<Animator>().SetTrigger("Play");
                    IndivPointsLabel = "";
                    break;
                }
            }
        }
    }
}
