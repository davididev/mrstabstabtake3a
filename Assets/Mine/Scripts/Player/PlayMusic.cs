﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    public AudioSource src;
    public static PlayMusic instance;

    public static float GlobalSoundVolume = 1f, MusicSoundVolume = 1f;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        src.ignoreListenerVolume = true;
        UpdateVolume();
    }

    public static void UpdateVolume()
    {
        instance.src.volume = MusicSoundVolume;
        AudioListener.volume = GlobalSoundVolume;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySong(AudioClip clip)
    {
        instance.src.clip = clip;
        instance.src.Play();
    }
}
