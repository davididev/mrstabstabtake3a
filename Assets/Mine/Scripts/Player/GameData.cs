﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData 
{
	[SerializeField]
	public class World
    {
		[SerializeField]
		public int[] highScores = { -1, -1, -1, -1, -1 };
		public bool isUnlocked = false;
		public World()
        {
			
        }

		public int MaxLevel
        {
			get
			{
				int v = 0;
				for(int i = 1; i < highScores.Length; i++)
                {
					if (highScores[i] != -1)
						v = i + 1;
				}
				return v;
            }
        }
    }

	[SerializeField] public World[] worlds;
	
	public static int GetMaxLevel(int world)
    {
		return GameDataHolder.instance.worlds[world].MaxLevel;
    }

	public GameData()
    {
		worlds = new World[5];
		for(int i = 0; i < worlds.Length; i++)
        {
			worlds[i] = new World();
        }

		worlds[0].isUnlocked = true;
    }

}
