﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneVars : MonoBehaviour
{
    private static string lastSong = "";
    public GameObject boilerplatePrefab;
    public AudioClip musicInScene;
    public TextAsset dialogueOnStart;
    public GameObject foodPrefab, deadPrefab;
    // Start is called before the first frame update
    void Start()
    {
        if(lastSong != musicInScene.name)
        {
            if (musicInScene != null)
                Invoke("PlaySong", Time.deltaTime);
        }
        

        if (BoilerRoot.Count == 0)
        {
            GameObject g = GameObject.Instantiate(boilerplatePrefab, Vector3.zero, Quaternion.identity);
        }

        if(dialogueOnStart != null)
        {
            StartCoroutine(Dialogue());
        }

        if (foodPrefab != null)
        {
            GameObjectPool.InitPoolItem("Food", foodPrefab, 90);
            if (PromptNotif.FirstPreGrab == false)
            {
                HealthOverlay.GuideLabel = "Grab both weapons to start the level.";
                PromptNotif.FirstPreGrab = true;
            }

        }
        if(deadPrefab != null)
        {
            GameObjectPool.InitPoolItem("Ded", deadPrefab, 10);
        }
            
    }

    IEnumerator Dialogue()
    {
        while(Mathf.Approximately(PlayerMovement.standingY, 0))
        {
            yield return new WaitForSecondsNoTimeScale(0.1f);
        }
        yield return new WaitForSecondsNoTimeScale(0.1f);

        DialogueHandler.GetInstance().StartEvent(dialogueOnStart);
    }

    void PlaySong()
    {
        lastSong = musicInScene.name;
        PlayMusic.PlaySong(musicInScene);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
