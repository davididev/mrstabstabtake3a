﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
#if UNITY_EDITOR || UNITY_STANDALONE
using Valve.VR;
#endif

public class CrossVR : MonoBehaviour
{
    [Space(20)]
    [Header("Steam VR Settings")]
    public GameObject steamVRRoot;
    public GameObject steamVRHead;
    public GameObject steamLeftHand;
    public GameObject steamRightHand;

#if UNITY_EDITOR || UNITY_STANDALONE
    public SteamVR_Action_Vector2 onJoysticksUpdate;
    public SteamVR_Action_Boolean onGripPressed, onTriggerPressed, onMenuPressed;
    public SteamVR_Action_Pose handPose, headPose;
    public SteamVR_Action_Vibration vibrateSource;

    private static SteamVR_Action_Vibration vibrateInstance;
#endif

    [Space(20)]
    [Header("Nolo VR Settings")]
    public GameObject questVRRoot;
    public GameObject questVRHead;
    public GameObject questLeftHand;
    public GameObject questRightHand;


    [Space(20)]
    [Header("Cross Settings")]
    public Transform crossLeftHand;
    public Transform crossRightHand;
    public Transform playerUI;

    public static Transform CurrentRoot = null, CurrentHMD = null, LeftHand = null, RightHand = null;


    public class Hand
    {
        public Hand()
        {

        }


        public bool triggerUp = false;
        public bool triggerDown = false;
        public bool triggerPressed = false;
        public bool gripUp = false;
        public bool gripDown = false;
        public bool gripPressed = false;
        public bool menuUp = false;
        public bool menuDown = false;
        public bool menuPressed = false;
        public Vector2 joystick;

        public Vector3 velocity;
    }

    public static Hand Left = new Hand();
    public static Hand Right = new Hand();

    // Start is called before the first frame update
    void Start()
    {
		
        if (Application.platform == RuntimePlatform.Android)
        {
            //Use nolo
            questVRRoot.SetActive(true);
            steamVRRoot.SetActive(false);


            crossLeftHand.parent = questLeftHand.transform;
            crossRightHand.parent = questRightHand.transform;

            CurrentRoot = questVRRoot.transform;
            CurrentHMD = questVRHead.transform;

        }
        else
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            vibrateInstance = vibrateSource;
#endif
            //Use Steam VR
            questVRRoot.SetActive(false);
            steamVRRoot.SetActive(true);


            crossLeftHand.parent = steamLeftHand.transform;
            crossRightHand.parent = steamRightHand.transform;


            CurrentRoot = steamVRRoot.transform;
            CurrentHMD = steamVRHead.transform;
        }

        LeftHand = crossLeftHand;
        RightHand = crossRightHand;
        Vector3 v = playerUI.localPosition;
        playerUI.parent = CurrentHMD;
        playerUI.localPosition = v;
        playerUI.localEulerAngles = Vector3.zero;
		XRDevice.DisableAutoXRCameraTracking(CurrentHMD.GetComponent<Camera>(), false);
    }

    static float vibrationTimerLeft = 0f;
    static float vibrationFreqLeft = 0f;
    static float vibrationAmpLeft = 0f;
    static float vibrationTimerRight = 0f;
    static float vibrationFreqRight = 0f;
    static float vibrationAmpRight = 0f;


    /// <summary>
    /// Cast a vibartion
    /// </summary>
    /// <param name="isLeftHand">Is it the left or the right?</param>
    /// <param name="frequency">Should be 0-1</param>
    /// <param name="amplitude">Should be 0-1.</param>
    public static void Vibrate(bool isLeftHand, float frequency, float amplitude, float timer = 1f)
    {
        if (Application.platform == RuntimePlatform.Android)
        {

            if (isLeftHand)
            {
                vibrationTimerLeft = timer / 2f;
                vibrationFreqLeft = frequency;
                vibrationAmpLeft = amplitude;
            }
            else
            {
                vibrationTimerRight = timer / 2f;
                vibrationFreqRight = frequency;
                vibrationAmpRight = amplitude;
            }
        }
        else
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            if (isLeftHand)
                vibrateInstance.Execute(0f, timer, frequency * 320f, amplitude, SteamVR_Input_Sources.LeftHand);
            else
                vibrateInstance.Execute(0f, timer, frequency * 320f, amplitude, SteamVR_Input_Sources.RightHand);
#endif
        }
            
    }

    // Update is called once per frame

    Camera cam;
    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            
            //Use Quest
            if (CurrentRoot == null)
                CurrentRoot = questVRRoot.transform;
            if (CurrentHMD == null)
                CurrentHMD = questVRHead.transform;


            if (cam == null)
                cam = questVRHead.GetComponent<Camera>();
            XRDevice.DisableAutoXRCameraTracking(cam, true);
            questVRHead.transform.position = CurrentHMD.TransformPoint(OVRManager.tracker.GetPose().position);
            //questVRHead.transform.rotation = OVRManager.tracker.GetPose().orientation;

            if (vibrationTimerLeft > 0f)
            {
                vibrationTimerLeft -= Time.deltaTime;
                OVRInput.SetControllerVibration(vibrationFreqLeft, vibrationAmpLeft, OVRInput.Controller.LTouch);
            }
            if (vibrationTimerRight > 0f)
            {
                vibrationTimerRight -= Time.deltaTime;
                OVRInput.SetControllerVibration(vibrationFreqRight, vibrationAmpRight, OVRInput.Controller.RTouch);
            }

            Right.triggerUp = OVRInput.GetUp(OVRInput.Button.SecondaryIndexTrigger);
            Right.triggerDown = OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger);
            Right.triggerPressed = OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger);
            Right.gripUp = OVRInput.GetUp(OVRInput.Button.SecondaryHandTrigger);
            Right.gripDown = OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger);
            Right.gripPressed = OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger);
            Right.menuUp = OVRInput.GetUp(OVRInput.Button.Two);
            Right.menuDown = OVRInput.GetDown(OVRInput.Button.Two);
            Right.menuPressed = OVRInput.Get(OVRInput.Button.Two);
            Right.joystick = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);
            Right.velocity = RightHand.TransformDirection(OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RHand));

            Left.triggerUp = OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger);
            Left.triggerDown = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger);
            Left.triggerPressed = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger);
            Left.gripUp = OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger);
            Left.gripDown = OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger);
            Left.gripPressed = OVRInput.Get(OVRInput.Button.PrimaryHandTrigger);
            Left.menuUp = OVRInput.GetUp(OVRInput.Button.Four);
            Left.menuDown = OVRInput.GetDown(OVRInput.Button.Four);
            Left.menuPressed = OVRInput.Get(OVRInput.Button.Four);
            Left.joystick = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
            Left.velocity = LeftHand.TransformDirection(OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LHand));
        }
        else
        {
            //Use Steam VR
            if (CurrentRoot == null)
                CurrentRoot = steamVRRoot.transform;
            if (CurrentHMD == null)
                CurrentHMD = steamVRHead.transform;
#if UNITY_EDITOR || UNITY_STANDALONE

            if (cam == null)
                cam = steamVRHead.GetComponent<Camera>();
            XRDevice.DisableAutoXRCameraTracking(cam, true);
            steamVRHead.transform.position = CurrentHMD.TransformPoint(headPose.localPosition);
            //steamVRHead.transform.rotation = headPose.localRotation;
			
            Left.gripDown = onGripPressed.GetStateDown(SteamVR_Input_Sources.LeftHand);
            Left.gripUp = onGripPressed.GetStateUp(SteamVR_Input_Sources.LeftHand);
            Left.gripPressed = onGripPressed.GetState(SteamVR_Input_Sources.LeftHand);
            Left.menuDown = onMenuPressed.GetStateDown(SteamVR_Input_Sources.LeftHand);
            Left.menuUp = onMenuPressed.GetStateUp(SteamVR_Input_Sources.LeftHand);
            Left.menuPressed = onMenuPressed.GetState(SteamVR_Input_Sources.LeftHand);
            Left.triggerDown = onTriggerPressed.GetStateDown(SteamVR_Input_Sources.LeftHand);
            Left.triggerUp = onTriggerPressed.GetStateUp(SteamVR_Input_Sources.LeftHand);
            Left.triggerPressed = onTriggerPressed.GetState(SteamVR_Input_Sources.LeftHand);
            Left.velocity = LeftHand.TransformDirection(handPose.GetVelocity(SteamVR_Input_Sources.LeftHand));
            Left.joystick = onJoysticksUpdate.GetAxis(SteamVR_Input_Sources.LeftHand);


            Right.gripDown = onGripPressed.GetStateDown(SteamVR_Input_Sources.RightHand);
            Right.gripUp = onGripPressed.GetStateUp(SteamVR_Input_Sources.RightHand);
            Right.gripPressed = onGripPressed.GetState(SteamVR_Input_Sources.RightHand);
            Right.menuDown = onMenuPressed.GetStateDown(SteamVR_Input_Sources.RightHand);
            Right.menuUp = onMenuPressed.GetStateUp(SteamVR_Input_Sources.RightHand);
            Right.menuPressed = onMenuPressed.GetState(SteamVR_Input_Sources.RightHand);
            Right.triggerDown = onTriggerPressed.GetStateDown(SteamVR_Input_Sources.RightHand);
            Right.triggerUp = onTriggerPressed.GetStateUp(SteamVR_Input_Sources.RightHand);
            Right.triggerPressed = onTriggerPressed.GetState(SteamVR_Input_Sources.RightHand);
            Right.velocity = RightHand.TransformDirection(handPose.GetVelocity(SteamVR_Input_Sources.RightHand));
            Right.joystick = onJoysticksUpdate.GetAxis(SteamVR_Input_Sources.RightHand);
#endif
        }
		if(firstUpdate)
		{
			firstUpdate = false;
			RightHand.transform.localPosition = Vector3.zero;
			LeftHand.transform.localPosition = Vector3.zero;
		}
		
    }

	bool firstUpdate = true;
	
}
