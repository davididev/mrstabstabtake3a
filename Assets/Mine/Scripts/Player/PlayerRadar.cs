using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerRadar : MonoBehaviour
{
    public const float MAX_RADIUS = 15f;
    public Transform playerMovementPosition;

    public RectTransform[] radars;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        Collider[] c = Physics.OverlapSphere(playerMovementPosition.position, MAX_RADIUS, LayerMask.GetMask("Enemy"));
        for(int i = 0; i < radars.Length; i++)
        {
            radars[i].gameObject.SetActive(i < c.Length);
            if(i < c.Length)
            {
                Vector3 v = playerMovementPosition.InverseTransformPoint(c[i].transform.position);
                radars[i].localPosition = new Vector3(v.x, v.z, 0f) * 64f / MAX_RADIUS;
            }
        }
    }
}
