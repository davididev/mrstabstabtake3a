using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    public enum CONNECTED_HAND { None, Left, Right };
    public CONNECTED_HAND connect = CONNECTED_HAND.None;
    public bool isGripped = false;
    private const float MIN_VELOCITY = 1.7f, MIN_ANGULAR = 1.25f;
    public BoxCollider bladeBox;

    public TrailRenderer trailRend;
    private Rigidbody rigid;
    bool isTelekinesis = false;
    public AudioClip whooshFX;

    private AudioSource audioSrc;

    private float whooshTimer = 0f;
    private const float DELAY_BETWEEN_WHOOSH = 0.2f;

    public static int GRIP_COUNT = 0;
    public static bool StartedLevel = false;
    public GameObject clangPrefab;

    // Start is called before the first frame update
    void Start()
    {
        StartedLevel = false;
        GRIP_COUNT = 0;
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();

        audioSrc = gameObject.AddComponent<AudioSource>();
        audioSrc.minDistance = 5f;

        if (clangPrefab != null)
            GameObjectPool.InitPoolItem("Clang", clangPrefab, 15);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();

        trailRend.emitting = false;
        if(rigid.velocity.magnitude > MIN_VELOCITY)
            trailRend.emitting = true;
        if (rigid.angularVelocity.magnitude > MIN_ANGULAR)
            trailRend.emitting = true;

        if (whooshTimer > 0f)
            whooshTimer -= Time.deltaTime;

        if (trailRend.emitting == true)
        {
            if(whooshTimer <= 0f)
            {
                if(isGripped)
                {
                    audioSrc.clip = whooshFX;
                    audioSrc.Play();
                }
                
                whooshTimer = DELAY_BETWEEN_WHOOSH;
            }
            Collider[] cs = PhysicsExtensions.OverlapBox(bladeBox, LayerMask.GetMask("Enemy"));
            foreach(Collider c in cs)
            {
                Vector3 frc = rigid.velocity.normalized * 6.5f;
                c.gameObject.SendMessage("Damage", new DmgMessage(1f, frc, null), SendMessageOptions.DontRequireReceiver);
            }
        }

        if(isGripped == false && isTelekinesis == false)
        {
            //Check MP later
            if(PlayerHealth.Magic >= 2f)
            {
                if (connect == CONNECTED_HAND.Right && CrossVR.Right.triggerDown)
                {
                    PlayerHealth.Magic -= 2f;
                    isTelekinesis = true;
                }
                if (connect == CONNECTED_HAND.Left && CrossVR.Left.triggerDown)
                {
                    PlayerHealth.Magic -= 2f;
                    isTelekinesis = true;
                }
            }
            
        }

        if(isTelekinesis)
        {
            Vector3 v = rigid.position;
            if (connect == CONNECTED_HAND.Right)
                v = CrossVR.RightHand.position - v;
            if (connect == CONNECTED_HAND.Left)
                v = CrossVR.LeftHand.position - v;
            float frc = 8f;
            if (v.magnitude < 2f)
                frc = 2f;
            rigid.velocity = v.normalized * frc;
        }

    }

    void OnGrip(bool isLeftHand)
    {
        isGripped = true;
        if(isLeftHand)
            connect = CONNECTED_HAND.Left;
        else
            connect = CONNECTED_HAND.Right;

        rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Vector3.zero;
        isTelekinesis = false;
        GRIP_COUNT++;
        if(GRIP_COUNT == 2)
            StartedLevel = true;
    }

    void OnRelease(bool isLeftHand)
    {
        isGripped = false;
        //Don't release connect- they can still call it back with telekinesis
        GRIP_COUNT--;

        if (PromptNotif.FirstTelekinesis == false)
        {
            HealthOverlay.GuideLabel = "Press trigger to return weapon.  Watch your magic meter!";
            PromptNotif.FirstTelekinesis = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        for(int i = 0; i < collision.contactCount; i++)
        {
            int colLayer = collision.contacts[i].otherCollider.gameObject.layer;
            if (colLayer == 0)  //Default- geometry
            {
                GameObjectPool.GetInstance("Clang", collision.contacts[i].point, Quaternion.identity);
            }
            if (colLayer == LayerMask.NameToLayer("Decoration"))  //Default- geometry
            {
                GameObjectPool.GetInstance("Clang", collision.contacts[i].point, Quaternion.identity);
            }
        }
        
    }

}
