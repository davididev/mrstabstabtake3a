﻿using UnityEngine;
using System.Collections;
using System;

public class WaitForSecondsNoTimeScale : CustomYieldInstruction {

    float time = 0f;
    float startTime = 0f;
    public WaitForSecondsNoTimeScale(float t)
    {
        time = t;
        startTime = Time.realtimeSinceStartup;
    }

    public override bool keepWaiting
    {
        get
        {
            if(Time.realtimeSinceStartup > (startTime + time)) { return false; }
            else
            { return true;  }
        }
    }
}
