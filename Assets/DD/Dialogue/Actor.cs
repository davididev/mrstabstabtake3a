﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Actor : MonoBehaviour {

    public string actorName;
    public static Dictionary<string, Actor> actors = new Dictionary<string, Actor>();
    public float moveSpeed = 5f;
    public float MIN_DISTANCE = 2f;
    public float maxSpeed = 10f;
    public float rotateSpeed = 1800f;
    public bool ignoreY = true;
    public bool activeOnStart = true, applyGravity = false;
	[HideInInspector] public bool runningPath = false;
	
	public Animator anim;
	public bool automaticFootPlacement = false;
	
    private CharacterController rigid;

    private Queue<Vector3> path = new Queue<Vector3>();

	Vector3 leftHandPosition, rightHandPosition;

    // Use this for initialization
    void Start () {
        actors.Add(actorName, this);
        rigid = GetComponent<CharacterController>();
        gameObject.SetActive(activeOnStart);
		
		if(automaticFootPlacement)
			anim.feetPivotActive = 1f;
    }


	public void SetPatMode(int id)
	{
		if(firstPat == true)
			StopCoroutine(pattingRoutine);
		if(id == 0)
			pattingRoutine = StartCoroutine(HeadPatRight());
		if(id == 1)
			pattingRoutine = StartCoroutine(HeadPatLeft());
	}

	private Coroutine pattingRoutine;
	private bool firstPat = false;
	public IEnumerator HeadPatRight()
	{
		firstPat = true;
		Vector3 startingRight = rightHandPosition;
		float t = 1f;
		while(gameObject)
		{
			iTween.ValueTo(gameObject, iTween.Hash("from", startingRight, "to", CrossVR.CurrentHMD.position, "time", t, "onupdate", "SetRightHand"));
			yield return new WaitForSeconds(t);
			iTween.ValueTo(gameObject, iTween.Hash("from", rightHandPosition, "to", startingRight, "time", t, "onupdate", "SetRightHand"));
			yield return new WaitForSeconds(t);
		}
	}
	
	public IEnumerator HeadPatLeft()
	{
		firstPat = true;
		Vector3 startingLeft = leftHandPosition;
		float t = 1f;
		while(gameObject)
		{
			iTween.ValueTo(gameObject, iTween.Hash("from", startingLeft, "to", CrossVR.CurrentHMD.position, "time", t, "onupdate", "SetLeftHand"));
			yield return new WaitForSeconds(t);
			iTween.ValueTo(gameObject, iTween.Hash("from", leftHandPosition, "to", startingLeft, "time", t, "onupdate", "SetLeftHand"));
			yield return new WaitForSeconds(t);
		}
	}
	
	void SetRightHand(Vector3 v)
	{
		rightHandPosition = v;
	}
	void SetLeftHand(Vector3 v)
	{
		leftHandPosition = v;
	}
	
	void OnAnimatorIK(int layerIndex)
	{
		anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
        anim.SetIKPosition(AvatarIKGoal.RightHand, rightHandPosition);
		anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
        anim.SetIKPosition(AvatarIKGoal.LeftHand, leftHandPosition);
	}

    void OnDestroy()
    {
        actors.Remove(actorName);
    }
	
	// Update is called once per frame
	void Update () {
        if (applyGravity)
        {
            if (rigid.isGrounded)
                gravity = -.01f;

            gravity = gravity - 9.8f * Time.fixedDeltaTime;
            rigid.Move(Vector3.up * gravity);
        }
		
    }

    public void MoveActor(Vector3[] points)
    {

        foreach(Vector3 p in points)
        {
            path.Enqueue(p);
        }
        runningPath = true;
        StartCoroutine(RunPath());
    }
    

    IEnumerator RunPath()
    {
		runningPath = true;
        Debug.Log("path size: " + path.Count);
        while(path.Count > 0)
        {
            Vector3 pointToLookAt = path.Peek();

            float distance = 500f;
			bool finished = false;
            while (finished == false)
            {
                Vector3 center = rigid.center + transform.position;
                Quaternion q1 = Quaternion.LookRotation(pointToLookAt - center);
                //float targetRot = Mathf.Atan2(pointToLookAt.x - center.x, pointToLookAt.z - center.z) * Mathf.Rad2Deg;
                float targetRot = q1.eulerAngles.y;
                //if (ignoreY)
                    distance = Vector2.Distance(new Vector2(center.x, center.z), new Vector2(pointToLookAt.x, pointToLookAt.z));
                //else
                //    distance = Vector3.Distance(center, pointToLookAt);



                Vector3 rot = transform.eulerAngles;
                
                float moveAngle = Mathf.MoveTowardsAngle(rot.y, targetRot, rotateSpeed * Time.deltaTime);
                Debug.Log("Current: " + rot.y + "; Target: " + targetRot + "; moveAngle: " + moveAngle + "; distance: " + distance);
                rot.y = moveAngle;
                transform.eulerAngles = rot;

                float d = moveSpeed * Time.fixedDeltaTime;
                if (d > distance)
                    d = distance;

                Debug.Log(d);
				Vector3 moveLol = transform.forward * d;
                
                //rigid.AddRelativeForce(Vector3.forward * moveSpeed, ForceMode.Acceleration);

                if (!ignoreY)
				{
                    //rigid.AddForce(transform.up * moveSpeed * ((rigid.position.y < pointToLookAt.y ) ? 1f : -1f), ForceMode.Acceleration);
					float d2 = moveSpeed * Time.fixedDeltaTime;
					float maxLol = Mathf.Abs(center.y - pointToLookAt.y);
					if(d2 > maxLol)
						d2 = maxLol;
                    moveLol = moveLol + (Vector3.down * d2);
				}
				
				if(moveLol == Vector3.zero)
					finished = true;
                rigid.Move(moveLol);
				yield return new WaitForFixedUpdate();
            }

            Vector3 no = path.Dequeue();
        }
        Debug.Log("Path finished");

        runningPath = false;

        
    }

    float gravity = 0f;
}
