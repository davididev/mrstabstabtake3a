﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueFlash : DialogueItem {

    public float r, g, b, time;
    public bool waitUntilDone;
    public string flashSoundEffect;

    public DialogueFlash()
    {
        r = 1f;
        g = 1f;
        b = 1f;
        time = 1f;
    }

    public override IEnumerator Run()
    {
        breakpoint = waitUntilDone;
        yield return base.Run();
        if (waitUntilDone == false)
            this.completed = true;


		
        Color c1 = new Color(r, g, b, 0f);
        Color c2 = new Color(r, g, b, 1f);

        if(flashSoundEffect != "")
        {
            AudioClip c = Resources.Load<AudioClip>("snd/" + flashSoundEffect);
            DialogueHandler.dialogueStringSrc.clip = c;
            DialogueHandler.dialogueStringSrc.loop = false;
            DialogueHandler.dialogueStringSrc.Play();
        }

        iTween.ValueTo(handle.gameObject, iTween.Hash("from", c1, "to", c2, "time", time / 2f, "onupdate", "SetFlashColor"));
        yield return new WaitForSeconds(time / 2f);
        iTween.ValueTo(handle.gameObject, iTween.Hash("from", c2, "to", c1, "time", time / 2f, "onupdate", "SetFlashColor"));
        yield return new WaitForSeconds(time / 2f);

        this.completed = true;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        base.DrawEditor(verticalGroup);
        GUILayout.Label("Note: colors should be between 0.0 to 1.0");
        GUILayout.BeginHorizontal();
        r = this.FloatField("Red", r);
        g = this.FloatField("Green", g);
        b = this.FloatField("Blue", b);
        GUILayout.EndHorizontal();

        time = this.FloatField("Time: ", time);
        waitUntilDone = GUILayout.Toggle(waitUntilDone, "Wait until done: ");

        GUILayout.BeginHorizontal();
        GUILayout.Label("Sound effect (leave blank if none):");
        flashSoundEffect = GUILayout.TextField(flashSoundEffect);
        GUILayout.EndHorizontal();
    }
}
