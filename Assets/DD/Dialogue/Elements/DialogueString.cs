﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[System.Serializable]
public class DialogueString : DialogueItem {

    public string characterName = "";
    public string textToDisplay = "";
    public string sound = "";
	public float charactersPerSecond = 20f;
	public bool closeAfter = false;

    private const float WIDTH = 400f;
    private const float HEIGHT = 300f;
    public static string lastCharacterName = "";

    public DialogueString()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();

        handle.SetMessageY(1f);
        handle.characterImage.sprite = Resources.Load<Sprite>("Face/" + characterName);
        handle.characterImage.color = Color.white;
        string str = textToDisplay;

        DialogueHandler.dialogueStringSrc.clip = Resources.Load<AudioClip>("Voice/" + sound);
		DialogueHandler.dialogueStringSrc.Play();
		
        str = DialogueHandler.VariablesToString(str);

        int i = 0;
        while (i < str.Length)
        {
            
            if (i >= str.Length)
                i = str.Length - 1;

            if (str[i] == '<')
            {
                i = str.IndexOf('>', i) + 1;
            }
            
            handle.message.text = str.Substring(0, i);
            yield return new WaitForSecondsNoTimeScale(1f / charactersPerSecond);
            i++;

            
        }

        handle.message.text = str;


        


        yield return new WaitForSecondsNoTimeScale(1f);
		if(closeAfter)
			handle.SetMessageY(0f);
        completed = true;

    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Character name");
        characterName = GUILayout.TextField(characterName);
        GUILayout.Label("Voice Acting");
        sound = GUILayout.TextField(sound);
        GUILayout.Label("Text to display");
        textToDisplay = GUILayout.TextField(textToDisplay);
        charactersPerSecond = this.FloatField("Characters per second", charactersPerSecond);


		closeAfter = GUILayout.Toggle(closeAfter, "Close after?");
		
    }

    //Needed for the editor.  Returns the object tpye
    public override string ToString()
    {
        return "Talk (" + characterName + ")";
    }
}
