﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[XmlRoot("DialogueHolder")]
public class DialogueHolder {

    public DialogueItem[] items;
	
}
