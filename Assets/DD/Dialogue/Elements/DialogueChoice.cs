﻿using System.Collections;
using System.Xml.Serialization;
using UnityEngine;

public class DialogueChoice : DialogueItem
{
    public static int ChoiceID = -1;  //This should be set by the button
    public string promptHeader = "";
    //public string[] choices = new string[0];
    [System.Serializable]
    public class Choice
    {
        public string choiceStr = "";
        public int gotoLine = 0;
    }
    public Choice[] choices = new Choice[0];

    [XmlIgnore] private int currentChoiceID = 0;

    public DialogueChoice()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();
        //Startup
        GripController.uiMode = true;
        ChoiceID = -1;
        handle.SetMessageY(1f);
        handle.message.text = promptHeader;

        handle.characterImage.sprite = Resources.Load<Sprite>("Face/Davidi");
        float lastTimeScale = Time.timeScale;
        Time.timeScale = 1f / 360f;


        
        

        for (int i = 0; i < handle.choiceOverlays.Length; i++)
        {
            handle.choiceOverlays[i].SetActive(i < choices.Length);
            handle.choiceOverlays[i].transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = choices[i].choiceStr;
        }

        while (ChoiceID == -1)
        { yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime); }//So any dialogue before doesn't make the choice selected instantly

        

        //Clear choices after the choice
        for (int i = 0; i < handle.choiceOverlays.Length; i++)
        {
            handle.choiceOverlays[i].SetActive(false);
        }

        Time.timeScale = lastTimeScale;

        handle.GotoLine(choices[ChoiceID].gotoLine);
        GripController.uiMode = false;
        completed = true;
    }

    private int lastChoiceCount = 0;
    private int t = 0;  //t is set per OnGUI
    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Top prompt");
        promptHeader = GUILayout.TextField(promptHeader);
        t = this.IntField("choice count", t);
        if(t != lastChoiceCount)  //Change size
        {
            choices = null;
            choices = new Choice[t];
            for(int i = 0; i < choices.Length; i++)
            {
                choices[i] = new Choice();
            }
            lastChoiceCount = t;
        }

        for(int i = 0; i < choices.Length; i++)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Choice " + i);
            
            choices[i].choiceStr = GUILayout.TextField(choices[i].choiceStr);
            choices[i].gotoLine = this.IntField("Goto", choices[i].gotoLine);
            GUILayout.EndHorizontal();
        }
    }

    public override string ToString()
    {
        return "Set choice";
    }
}