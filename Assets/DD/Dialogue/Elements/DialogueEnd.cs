﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

public class DialogueEnd : DialogueItem
{

    public DialogueEnd()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();
        handle.SetMessageY(0f);
        DialogueHandler dh = Object.FindObjectOfType<DialogueHandler>();
        dh.EndEvent();
        completed = true;
        yield return null;
    }

    public override void DrawEditor(Rect verticalGroup)
    {

        GUILayout.Label("This element is not editable.");
    }

    //Needed for the editor.  Returns the object tpye
    public override string ToString()
    {
        return "End dialogue";
    }
}