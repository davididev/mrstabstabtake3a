﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayeredSkymaskScroller : MonoBehaviour
{
    public static float Layer2ScrollPerSecond = 0.1f, Layer3ScrollPerSecond = 0f, Layer1ScrollPerSecond = -0.1f;
    public float layer1AnglesPerSecond = -0.1f, layer2AnglesPerSecond = 0.1f, layer3AnglesPerSecond = 0f;
	public float layer1StartAngle = 0f, layer2StartAngle = 0f, layer3StartAngle = 0f;
    
	private float offsetLayerBase, offsetLayer1, offsetLayer2;
    // Start is called before the first frame update
    void Start()
    {
        
            Layer2ScrollPerSecond = layer2AnglesPerSecond;
        
            Layer3ScrollPerSecond = layer3AnglesPerSecond;
            Layer1ScrollPerSecond  = layer1AnglesPerSecond;
		
		offsetLayerBase = layer1StartAngle;
		offsetLayer1 = layer2StartAngle;
		offsetLayer2 = layer3StartAngle;
    }

    // Update is called once per frame
    void Update()
    {

		offsetLayerBase += Layer1ScrollPerSecond * Time.deltaTime;
		if(offsetLayer1 < 0f)
			offsetLayer1 += 360f;
		if(offsetLayer1 > 360f)
			offsetLayer1 -= 360f;


        offsetLayer1 += Layer2ScrollPerSecond * Time.deltaTime;
		if(offsetLayer1 < 0f)
			offsetLayer1 += 360f;
		if(offsetLayer1 > 360f)
			offsetLayer1 -= 360f;

        offsetLayer2 += Layer3ScrollPerSecond * Time.deltaTime;
		if(offsetLayer2 < 0f)
			offsetLayer2 += 360f;
		if(offsetLayer2 > 360f)
			offsetLayer2 -= 360f;
		
		RenderSettings.skybox.SetFloat("_Rotation", offsetLayerBase);
		RenderSettings.skybox.SetFloat("_Rotation2", offsetLayer1);
		RenderSettings.skybox.SetFloat("_Rotation3", offsetLayer2);
    }
}
